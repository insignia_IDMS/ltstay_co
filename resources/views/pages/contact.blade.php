<div class='row contact-us page'>
	<div class='heading-wrapper row'>
		<h2 class='heading'>Contact</h2>
	</div>

	@if(Session::has('success'))
		<div class="alert alert-success" style="text-align: center;">
			<strong>{{session('success')}}</strong>
		</div>
	@endif

	<div class='content-wrapper row'>
	  	<div class="col-xs-12 col-sm-12 col-md-5 col-left">
			<div class='row'>
				<h4 class='heading'> Email </h4>
				<div class="col-xs-12 col-sm-6 col-md-6 contact-left">
					<span>Customer Service</span>
				</div>
				<div class="col-xs-12 col-sm-6 col-md-6 contact-right">
					<a href="mailto:crm@ltstay.com">crm@ltstay.com</a>
				</div>
			</div>
			<div class='row'>
				<h4 class='heading'> Office </h4>
				<div class="col-xs-12 col-sm-6 col-md-6 contact-left">
					<span>Address</span>
				</div>
				<div class="col-xs-12 col-sm-6 col-md-6 contact-right">
					<span>1584 Branham Ln, 65, San Jose 95118</span>
					<span></span>
				</div>
				<div class="col-xs-12 col-sm-6 col-md-6 contact-left">
					<span>Phone</span>
				</div>
				<div class="col-xs-12 col-sm-6 col-md-6 contact-right">
					<span>1-844-4LTSTAY</span>
				</div>
			</div>
			<div class='row'>
				<h4 class='heading'> Social </h4>
				<div class="col-xs-12 col-sm-6 col-md-6 contact-left">
					<span>Facebook</span>
				</div>
				<div class="col-xs-12 col-sm-6 col-md-6 contact-right">
					<a href='https://www.facebook.com/longtermstay/'>https://www.facebook.com/longtermstay/</a>
				</div>
				<div class="col-xs-12 col-sm-6 col-md-6 contact-left">
					<span>Google+</span>
				</div>
				<div class="col-xs-12 col-sm-6 col-md-6 contact-right">
					<a href='https://plus.google.com/+Ltstay/about'>https://plus.google.com/+Ltstay/</a>
				</div>
				<div class="col-xs-12 col-sm-6 col-md-6 contact-left">
					<span>Twitter</span>
				</div>
				<div class="col-xs-12 col-sm-6 col-md-6 contact-right">
					<a href='https://twitter.com/uscorppg'>https://twitter.com/uscorppg</a>
				</div>
			</div>
		</div>
	  	<div class="col-xs-12 col-md-7 col-right">
			<div class='row'>
				<h4 class='heading'> Send us a note </h4>
				<div class='heading-subtext'>Comments or Questions? We'd love to hear from you!</div>
			</div>
			<form method='post' class='contact-form' action="{{url('contact')}}">
				<?php
                /*
					if (validation_errors()) {
						echo '<span class="contact-error">';
						echo validation_errors();
						echo '</span>';
					}
                */
				?>
			  	<div class="form-group">
					<div class='col-sm-12'>
						<label for="email">Email <span class="required">*</span></label>
					</div>
					<div class='col-sm-12'>
						<input name='email' type="email" class="must-have form-control" id="email" placeholder="Email" data-toggle="tooltip" data-placement="right" title="please enter email address. ex:crm@ltstay.com">
					</div>
			  	</div>
			  	<div class="form-group">
					<div class='col-sm-12'>
						<label for="name">Name <span class="required">*</span></label>
					</div>
					<div class='col-sm-12'>
						<input name='name' type="text" class="must-have form-control" id="name" placeholder="Name" placeholder="Email" data-toggle="tooltip" data-placement="right" title="please enter first name & last name. ex: John Appleseed. ex:crm@ltstay.com">
					</div>
			  	</div>
			  	<div class="form-group">
					<div class='col-sm-12'>
						<label for="message">Message <span class="required">*</span></label>
					</div>
					<div class='col-sm-12'>
						<textarea class="must-have form-control" id="message" name='message' placeHolder='Type your message here.' placeholder="Email" data-toggle="tooltip" data-placement="right" title="please enter email message here."></textarea>
					</div>
				</div>
				<div class='btn-wrapper row col-sm-12'>
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
					<button type='submit' class='btn btn-danger contact-btn'>Send Message</button>
				</div>
			</form>
		</div>
	</div>
</div>
