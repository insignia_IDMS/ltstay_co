<div class="help page">
	<div class='heading-wrapper row'>
		<h2 class='heading'> Help / Frequently Asked Questions&nbsp;</h2>
	</div>
	<div class='content-wrapper row'>
		<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
			<div class="panel panel-default">
				<div class="panel-heading" role="tab" id="heading1">
					<h4 class="panel-title">
						<a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse1" aria-expanded="true" aria-controls="collapse1">
							Why Long Term Stay?
						</a>
					</h4>
				</div>
				<div id="collapse1" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading1">
					<div class="panel-body">
						<p>Long Term Stay is your one stop shop for all thing “<em>Living</em>“. We are an aggregator for housing solutions, those offered from hosts and realtors including apartments, rooms and rental spaces or those directly managed by us with integrated&nbsp;service like commute, laundry, tour and travel, concierge, grocery delivery, food delivery offered through vendors and partners.</p>
						<p>Our mantra is help you concentrate on your core area of business and let us handle everything else in the most professional, hassle-free and tailor-made setting, something we are best at.</p>
						<p>As our customer, you will always find us on your side.</p>
						<div id="tabs-widget-wrap">
							<ul id="tabs">
								<li id="">
									<a href="#" name="#tab1" id="">As a Guest</a>
								</li>
								<li id="">
									<a href="#" name="#tab2" id="">As a Host</a>
								</li>
								<li id="current">
									<a href="#" name="#tab3" id="current">As a Corporate HR</a>
								</li>
							</ul>
							<div id="tabs-content">
								<div id="tab1" class="tab-content" style="display: none;">
									<p></p>
									<p>You will have a round the clock assistance made available to you through our customer relation team whether its fixing your streaming media channels, arranging for a mid night airport pickup and drop, getting the food and deliveries you have wanted or that extra hands you needed for arranging the furnitures and fixtures to your liking.</p>
									<p>We have our customer help portal at&nbsp;<a href="http://support.long-termstay.com/" onclick="__gaTracker('send', 'event', 'outbound-article', 'http://support.long-termstay.com/', 'Long Term Stay Support Portal');">Long Term Stay Support Portal</a></p>
									<p></p>
								</div>
								<div id="tab2" class="tab-content" style="display: none;"><p></p>
									<p>You will have us helping you get the right help at all times, even when you are vacationing miles away. Be it the Hospitality team doing the beds and that extra cleaning before your guest checks in or the plumbers fixing that bathroom clog at midnight to that of arranging the regular chauffeuring your guest needed to reach at work, not to forget the food service our vendors get you at huge discount at your guests dining area. You will always get pointers from us even for your accounting needs or leasing and legal help you ever need.</p>
									<p></p>
								</div>
								<div id="tab3" class="tab-content" style="display: block;"><p></p>
									<p>You will have us managing your employee’s accommodation or your leased guesthouses in the most cost-effective way. Ours is one model that your employees will vouch for over and above an extended hotel booking as it’s not just a room equipped with facility but a “Home” like living experience. Your folks will get a chance to share their day’s experience with new friends and co-guests while munching on the homely food provided, watching the latest movies and the ‘Quantico’s or “Big Boss”s or the physics in the “Big Bang Theory”.</p>
									<p></p>
								</div>
							</div>
						</div>
						<p><strong></strong></p><h3><strong> About Long Term Stay </strong></h3><p></p>
						<p>We are a Delaware corporation, operating 14 guesthouses directly managed by us in south bay area providing hospitality solutions to almost hundreds of folks working with leading companies like Apple, Google, Yahoo, Cisco, Facebook, Comcast, Motorola, HP, Samsung etc.</p>
						<p>We are an established brand having strong value system, customer focus and clear policies. Our mantra is help you concentrate on your core area of business and let us handle everything else in the most professional, hassle-free and tailor-made setting, something we are best at.</p>
					</div>
				</div>
			</div>
			<div class="panel panel-default">
				<div class="panel-heading" role="tab" id="heading2">
					<h4 class="panel-title">
						<a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse2" aria-expanded="true" aria-controls="collapse2">
							Why Choose Us?
						</a>
					</h4>
				</div>
				<div id="collapse2" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading2">
					<div class="panel-body">
						<p>Top reasons why our guests choose us are</p>
						<ul>
							<li>No lease or unwanted obligation. With us, you can choose your move-in and move-out dates exactly as you would want.</li>
							<li>Guesthouse themed like home, set-up entirely around what you need on a day-to-day basis. Compare it against grandiose, exorbitant and showcase-like settings in a hotel, something you may not need everyday of the month.</li>
							<li>A complete packaged solution for one fixed cost rather than having to shop every bit each time, be it consumables, appliances or furnishing</li>
							<li>Integrated Housekeeping, Cleaning, Concierge and Property Management services</li>
							<li>Indian Homely-Food and commute services through partners</li>
							<li>Friendly co-guests in a warm setup and onsite service manager when you need them</li>
						</ul>
						<p>You no longer need to hunt for an interested colleague sharing apartment or risk loosing a relation adjusting to others.</p>
						<p><strong><em> Life is good here!!!</em></strong><br></p>
					</div>
				</div>
			</div>

			<div class="panel panel-default">
				<div class="panel-heading" role="tab" id="heading3">
					<h4 class="panel-title">
						<a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse3" aria-expanded="true" aria-controls="collapse3">
							List of Edibles/General Consumables
						</a>
					</h4>
				</div>
				<div id="collapse3" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading3">
					<div class="panel-body">
						<p><strong>Edible Items</strong></p>
						<ul>
							<li>Fruits
								<ul>
									<li>Apple</li>
									<li>Oranges</li>
									<li>Grapes</li>
									<li>Banana</li>
								</ul>
							</li>
							<li>General Edibles
								<ul>
									<li>Bread</li>
									<li>Salt</li>
									<li>Sugar</li>
									<li>Milk</li>
									<li>Tea</li>
									<li>Coffee</li>
									<li>Pop Corn</li>
									<li>Tea Masala</li>
									<li>Cheese Spread</li>
									<li>Egg</li>
									<li>Poha</li>
									<li>Onion</li>
									<li>Potato</li>
									<li>Tomato</li>
									<li>Vegetable Oil</li>
									<li>Essential Kitchen Masala</li>
									<li>Chilly Sauce</li>
									<li>Tomato Sauce</li>
									<li>Curd</li>
								</ul>
							</li>
							<li>Cereals
								<ul>
									<li>Kellogs Special K</li>
									<li>Cheerios Plain/Multigrain</li>
									<li>Oat Meal</li>
									<li>Kellogs Honey bunches of oats</li>
								</ul>
							</li>
							<li>Juice and Drinks
								<ul>
									<li>Apple Juice</li>
									<li>Mango Juice</li>
									<li>Orange Juice</li>
									<li>Grapes Juice</li>
									<li>Pepsi or Coke</li>
								</ul>
							</li>
							<li>Others
								<ul>
									<li>Namkeen</li>
									<li>Mathari</li>
									<li>Dosa Batter</li>
									<li>Pickles</li>
									<li>Jams</li>
								</ul>
							</li>
						</ul>
						<p><strong>General Consumables</strong></p>
						<ul>
							<li>Toiletries
								<ul>
									<li>Tooth Paste</li>
									<li>Tooth Brush</li>
									<li>Shaving Gel</li>
									<li>Shampoo</li>
									<li>Body Wash</li>
									<li>Conditioner</li>
									<li>Mouth Wash</li>
									<li>Moisturizer</li>
									<li>Bath Tissues</li>
								</ul>
							</li>
							<li>&nbsp;Supplies
								<ul>
									<li>Kitchen Towels</li>
									<li>Dish Washing Detergent</li>
									<li>Dishwasher Detergent</li>
									<li>Laundry Detergent</li>
									<li>Toilet Bowl Cleaner</li>
									<li>Room Freshener Spray</li>
									<li>Room Freshener oil and heater</li>
									<li>Drain buster</li>
									<li>Water Purifier</li>
								</ul>
							</li>
						</ul>
					</div>
				</div>
			</div>
			<div class="panel panel-default">
				<div class="panel-heading" role="tab" id="heading4">
					<h4 class="panel-title">
						<a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse4" aria-expanded="true" aria-controls="collapse4">
							Where are we located?
						</a>
					</h4>
				</div>
				<div id="collapse4" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading4">
					<div class="panel-body">
						<p><strong>Current Locations</strong></p>
						<ul>
							<li>Sunnyvale</li>
							<li>Santa Clara</li>
							<li>Milpitas</li>
							<li>San Jose</li>
							<li>Mountain View</li>
							<li>Cupertino</li>
						</ul>
						<p><strong>Upcoming locations</strong></p>
						<ul>
							<li>Fremont</li>
							<li>Dallas Fort Worth Area (2016)</li>
						</ul>
					</div>
				</div>
			</div>

			<div class="panel panel-default">
				<div class="panel-heading" role="tab" id="heading5">
					<h4 class="panel-title">
						<a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse5" aria-expanded="true" aria-controls="collapse5">
							Service Offerings
						</a>
					</h4>
				</div>
				<div id="collapse5" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading5">
					<div class="panel-body">
						<p>We have fully integrated service bouquet for your comfortable stay.</p>
						<ul>
							<li>Fully Furnished Guesthouses with single and shared rooms.</li>
							<li>TV, Sofa, Dinning table etc in common area.</li>
							<li>Table, executive chair, Bedding, Bed , Closets in rooms.</li>
							<li>High Speed 100 Mbps WI-Fi , Indian Channels, Roku, Netflix.</li>
							<li>Washer, dryer, iron box, kitchen appliances etc.</li>
							<li>Professional Weekly Cleaning, onsite service manager.</li>
							<li>Complementary Breakfast items inclusive of almost everything you might need as fruit, juice, milk, cereals, bread, egg, butter, jam, spreads etc;</li>
							<li>General Consumables right from shaving gel, toothbrush, toiletries, laundry detergent, chemicals, tissues, room fresheners etc;</li>
						</ul>
					</div>
				</div>
			</div>

			<div class="panel panel-default">
				<div class="panel-heading" role="tab" id="heading6">
					<h4 class="panel-title">
						<a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse6" aria-expanded="true" aria-controls="collapse6">
							Why don’t we show addresses of properties on our Ad/Facebook page?
						</a>
					</h4>
				</div>
				<div id="collapse6" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading6">
					<div class="panel-body">
						<br>
							Long Term Stay takes care of your privacy and of others in neighborhood. This is especially more important for our female guests.
						<br>
						You will get access to all the available options along with every bit of information and pointers you will need once we have validated your identity and credentials.
						<p>Please check out the properties listing for approximate addresses of current properties.</p>
					</div>
				</div>
			</div>

			<div class="panel panel-default">
				<div class="panel-heading" role="tab" id="heading7">
					<h4 class="panel-title">
						<a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse7" aria-expanded="true" aria-controls="collapse7">
							Is there any lease required?
						</a>
					</h4>
				</div>
				<div id="collapse7" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading7">
					<div class="panel-body">
						Long Term Stay does not lease its properties. You are required to sign up a contract detailing your intent to stay as a guest for the specified period. Your period of stay is flexible and for example, can range from a month to several years on month-to- month contract.
						<p>You are still expected to provide a notice period of 30 days for month-to-month contract unless you have already provided a definite move-out date at the start of your stay.</p>
					</div>
				</div>
			</div>

			<div class="panel panel-default">
				<div class="panel-heading" role="tab" id="heading8">
					<h4 class="panel-title">
						<a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse8" aria-expanded="true" aria-controls="collapse8">
							I am a female/first time traveller to Bay Area and am a little concerned!!
						</a>
					</h4>
				</div>
				<div id="collapse8" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading8">
					<div class="panel-body">
						We fully understand that your confidence in our brand, our value system and customer is our most important asset.<br>
						We strive to constantly evolve ourselves to align closer to your needs and have clear, fair and standard policies. As an example, we have a soft notice period policy; soft move-out date policy and you buy-we reimburse policy, to name a few.
						<p>Our female only guesthouses will have very limited access even to our male employees. What’s more, we have options of safe box and insurance for our guests. Please check with onsite customer service representative for details.</p>
						<p>Please go through the recommendations we earned on our facebook page https://www.facebook.com/pages/longtermstay</p>
					</div>
				</div>
			</div>

			<div class="panel panel-default">
				<div class="panel-heading" role="tab" id="heading9">
					<h4 class="panel-title">
						<a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse9" aria-expanded="true" aria-controls="collapse9">
							Why do we need a deposit?
						</a>
					</h4>
				</div>
				<div id="collapse9" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading9">
					<div class="panel-body">
						<br>
						Deposits are required from all guests irrespective of their duration of stay. The deposit amount is a safeguard against any intentional damage to property. It also serves as a tool to enforce the notice period requirement.<p></p>
						<p>Your deposit will be refunded to you fully within 7 days of your move out date or as agreed.</p>
						<p></p>
					</div>
				</div>
			</div>

			<div class="panel panel-default">
				<div class="panel-heading" role="tab" id="heading10">
					<h4 class="panel-title">
						<a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse10" aria-expanded="true" aria-controls="collapse10">
							My company may suddenly ask me to move out
						</a>
					</h4>
				</div>
				<div id="collapse10" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading10">
					<div class="panel-body">
						<br>
						Trust us, we do not want to turn your adversity to our gain.<p></p>
						<p>Please keep us informed of any such eventuality as soon as you can and we will try to get a replacement as closer as possible to your new move-out date (even you can help, if possible). We are always considerate of the unexpected turn of events and will have a discounted charges.</p>
						<p></p>
					</div>
				</div>
			</div>

			<div class="panel panel-default">
				<div class="panel-heading" role="tab" id="heading11">
					<h4 class="panel-title">
						<a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse11" aria-expanded="true" aria-controls="collapse11">
							What all is provided? Do I need to carry my own stuff?
						</a>
					</h4>
				</div>
				<div id="collapse11" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading11">
					<div class="panel-body">
						<br>
						Our business model is much more than a general bed and breakfast model. We strive to make your move-in and stay as hassle-free as possible.<p></p>
						<p>We will provide you a fully furnished room, living room and kitchen including all appliances and goods as possible.</p>
						<p><strong>Kitchen</strong></p>
						<p>• Microwave<br>
						• Refrigerator<br>
						• Essential Cookware<br>
						• Cooking range<br>
						• Toaster, Mixer, coffee maker<br>
						• Water Purifier<br>
						• Dinnerware and silverware</p>
						<p><strong> Room</strong></p>
						<p>• Table<br>
						• Fan<br>
						• Heater<br>
						• Bedding and comforter set<br>
						• Mattress, bed-frame and pillows<br>
						• Wardrobe</p>
						<p><strong> Living and Dinning Room</strong></p>
						<p>• TV<br>
						• Streaming Player<br>
						• Wi-Fi router<br>
						• Sofa and coffee table<br>
						• Organizers<br>
						• Dining table and chairs<br>
						• Other furnishing</p>
						<p>Along with washer and dryer, water heater etc. Any capital goods not listed here but deemed a necessity by majority of guests will also be provided.</p>
						<p>On top of this, you are provided general consumables right from shaving gel, toothbrush, toothpaste, toiletries to laundry detergent and breakfast items like milk, bread, egg, onions, cooking oil, fruit, juice, pickles etc. complementary to your stay.</p>
						<p>Essentially the only thing you may need is your suitcase and personal cosmetic.</p>
						<p></p>
					</div>
				</div>
			</div>

			<div class="panel panel-default">
				<div class="panel-heading" role="tab" id="heading12">
					<h4 class="panel-title">
						<a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse12" aria-expanded="true" aria-controls="collapse12">
							What’s you-buy-we-reimburse policy?
						</a>
					</h4>
				</div>
				<div id="collapse12" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading12">
					<div class="panel-body">
						<br>
						We have optimized our consumable replenishment based on experience hosting thousands of our guests so far. Still, plans can fail and you may end up with a refrigerator out of bread late at night.<p></p>
						<p>You have the right to buy such replenishment on your own and every such purchase (of listed items) will be reimbursed.</p>
					</div>
				</div>
			</div>

			<div class="panel panel-default">
				<div class="panel-heading" role="tab" id="heading13">
					<h4 class="panel-title">
						<a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse13" aria-expanded="true" aria-controls="collapse13">
							Anything more?
						</a>
					</h4>
				</div>
				<div id="collapse13" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading13">
					<div class="panel-body">
						<br>
						Cleanliness is a must in a shared living environment, so we include a weekly cleaning by professionals complementary to your stay. Every inch of space is covered.<p></p>
						<p>We have tied up with leading pest control agencies for periodic treatment of the guesthouse and surroundings.</p>
						<p>All utilities including Internet, media-channels, electricity, water, garbage disposal etc. is included. Ample Car parking is available on driveway and street parking. Guests are expected to follow the city and community rules and are fully responsible for their vehicles.</p>
						<p></p>
					</div>
				</div>
			</div>

			<div class="panel panel-default">
				<div class="panel-heading" role="tab" id="heading14">
					<h4 class="panel-title">
						<a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse14" aria-expanded="true" aria-controls="collapse14">
							Still More??
						</a>
					</h4>
				</div>
				<div id="collapse14" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading14">
					<div class="panel-body">
						<br>
						We are always on your side and understand your needs as much as you do. We have partnered with preferred vendors to take care of two of your other needs supplementing your stay i.e. your food and your commute.<p></p>
						<p>We have vendors providing food in your office and at home for lunch, dinner and evening snacks.<br>
						Howsoever much we would want, there is still some distance between your office and your new home. For your comfort, we have arranged for pickup and drop facility from/to your office and home cutting down the entire distance and/or the hassle of looking up at the bus/train routes and walkways.</p>
						<p>Please note that the food and pickup and drop are partner services, which are independent of Long Term Stay. We have just worked out a dream deal, again, being on your side.</p>
					</div>
				</div>
			</div>

			<div class="panel panel-default">
				<div class="panel-heading" role="tab" id="heading15">
					<h4 class="panel-title">
						<a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse15" aria-expanded="true" aria-controls="collapse15">
							Finally, how do I book my stay?
						</a>
					</h4>
				</div>
				<div id="collapse15" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading15">
					<div class="panel-body">
						<p></p>
						<p>You can use the room reservation on this site or you may kindly call us toll-free at 1-844-4LTSTAY.</p>
						<p>Folks visiting bay area from India can avail service of our Indian call center located in Hyderabad</p>
						<p></p>
					</div>
				</div>
			</div>
			<div class="panel">
				<div class="panel-collapse collapse in">
					<div class="panel-body">
						<div>Let us help save you the trouble of house hunting and the myriad of unknowns/variables. Just walk in with your suitcase and enjoy the unparalleled facilities on offer.</div>
					</div>
				</div>
			</div>
		<div>
	</div>
	</div>
</div>
