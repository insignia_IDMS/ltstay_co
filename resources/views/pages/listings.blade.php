<?php
//echo "<pre>";
//print_r($data['listings']);
//exit;
$listings = $data['listings'];
?>

<div class="listings page row">

<form id="listings-form" action="{{url('/')}}/select" method="post" accept-charset="utf-8">
	<?php
		if (count($listings)) {
			echo '';

			echo "<input type='hidden' name='slip[]' id='slip'/>";
			foreach($listings as $key => $value) {
				echo '<div class="row listings-item">';
					echo '<div class="row listings-main">';
						echo '<div class="col-xs-12 col-sm-3 col-md-3">';
							if(count($value['image'])) {
								foreach($value['image'] as $key => $img) {
									echo "<a href='http://longtermstay.checkfront.com/{$img['path']}' data-toggle='lightbox'>";
										echo "<img src='http://longtermstay.checkfront.com/{$img['path']}' class='listings-img'></img>";
									echo "</a>";
									break;
								}
							} else {
								echo '<i class="fa fa-picture-o fa-5" aria-hidden="true"></i>';
							}
						echo '</div>';
						echo '<div class="col-xs-12 col-sm-9 col-md-9">';
							echo '<div class="row">';
								echo '<div class="col-xs-12 col-sm-9 col-md-9">';
									echo "<a class='listings-name-wrapper' role='button' data-toggle='collapse' href='#item{$value['item_id']}' aria-expanded='false' aria-controls='collapseExample'>";
										echo '<div class="listings-name row">';
											echo $value['name'].' - '.$value['category_name'];
										echo '</div>';
										echo '<div class="row">';
											echo $value['price_details'];
										echo '</div>';
										echo '<div class="row">';
											echo $value['date'];
										echo '</div>';
										echo '<div class="row">';
											echo '<span class="btn-link">Show Details</span>';
										echo '</div>';
									echo '</a>';
								echo '</div>';
								echo '<div class="listings-price-wrapper col-xs-12 col-sm-3 col-md-3">';
									echo '<div class="listings-price row">';
										echo $value['price'];
									echo '</div>';
									echo '<div class="row listings-price-small">';
										echo ' '.$value['price_unit'];
									echo '</div>';
									echo '<div class="row">';
										echo '<input type="button" class="btn btn-primary listings-select-btn" value="Select" data-slip="'.$value['slip'].'"/>';
									echo '</div>';
								echo '</div>';
							echo '</div>';
						echo '</div>';
					echo '</div>';
					echo "<div id='item{$value['item_id']}' class='row collapse listings-more'>";
						echo '<div class="col-xs-12 col-sm-6 col-md-9">';
							echo '<ul class="nav nav-tabs" role="tablist">';
								echo '<li role="presentation" class="active">';
									echo "<a href='#details{$value['item_id']}' aria-controls='details' role='tab' data-toggle='tab'>Details</a>";
								echo '</li>';
								echo '<li role="presentation">';
									echo "<a href='#map{$value['item_id']}' aria-controls='map' role='tab' data-toggle='tab'>Map</a>";
								echo '</li>';
								echo '<li role="presentation">';
									echo "<a href='#features{$value['item_id']}' aria-controls='features' role='tab' data-toggle='tab'>Features</a>";
								echo '</li>';
								echo '<li role="presentation">';
									echo "<a href='#policies{$value['item_id']}' aria-controls='policies' role='tab' data-toggle='tab'>Policies</a>";
								echo '</li>';
							echo '</ul>';
							echo '<div class="tab-content">';
								echo "<div role='tabpanel' class='tab-pane active' id='details{$value['item_id']}'>";
									echo '<p class="listings-more-content">Extending the long term stay option for families with possibly a small kid. These spacious rooms are designed for comfort and relaxation. The bathroom will be exclusively provided for families with hot & cold rain shower and complimentary toiletries as standard. Fully-air conditioned in most homes, our rooms have ample closet space and luggage rack for your personal belongings, a desk and executive chair and all furnishings for your convenience. Missing anything.. Just ask us.</p>';
								echo '</div>';
								echo "<div role='tabpanel' class='tab-pane' id='map{$value['item_id']}'>";
									echo '<div class="listings-more-content">';
									if ($value['location'] != "") {
										echo "<a href='{$value['location']['link']}'>{$value['location']['link_txt']}</a>";
									} else {
										echo "Map not available at this time.";
									}
									echo '</div>';
								echo '</div>';
								echo "<div role='row tabpanel' class='tab-pane' id='features{$value['item_id']}'>";
									echo '<ul class="col-xs-12 col-sm-6 col-md-6 listings-more-content">';
										echo '<li>';
											echo '<i class="fa fa-star" aria-hidden="true"></i>';
											echo '&nbsp;&nbsp;';
											echo 'Free Wi-Fi Internet inside rooms';
										echo '</li>';
										echo '<li>';
											echo '<i class="fa fa-star" aria-hidden="true"></i>';
											echo '&nbsp;&nbsp;';
											echo 'Breakfast Items included';
										echo '</li>';
										echo '<li>';
											echo '<i class="fa fa-star" aria-hidden="true"></i>';
											echo '&nbsp;&nbsp;';
											echo 'Airport Pickup and Drop';
										echo '</li>';
										echo '<li>';
											echo '<i class="fa fa-star" aria-hidden="true"></i>';
											echo '&nbsp;&nbsp;';
											echo 'Satelite and Cable TV Included';
										echo '</li>';
										echo '<li>';
											echo '<i class="fa fa-star" aria-hidden="true"></i>';
											echo '&nbsp;&nbsp;';
											echo 'Appliances';
										echo '</li>';
									echo '</ul>';
									echo '<ul class="col-xs-12 col-sm-6 col-md-6 listings-more-content">';
										echo '<li>';
											echo '<i class="fa fa-star" aria-hidden="true"></i>';
											echo '&nbsp;&nbsp;';
											echo 'Room Cleaning Services';
										echo '</li>';
										echo '<li>';
											echo '<i class="fa fa-star" aria-hidden="true"></i>';
											echo '&nbsp;&nbsp;';
											echo 'Beautiful view on the city';
										echo '</li>';
										echo '<li>';
											echo '<i class="fa fa-star" aria-hidden="true"></i>';
											echo '&nbsp;&nbsp;';
											echo 'Free parking on Driveway, Street';
										echo '</li>';
										echo '<li>';
											echo '<i class="fa fa-star" aria-hidden="true"></i>';
											echo '&nbsp;&nbsp;';
											echo 'Air Conditioning';
										echo '</li>';
										echo '<li>';
											echo '<i class="fa fa-star" aria-hidden="true"></i>';
											echo '&nbsp;&nbsp;';
											echo 'Refrigerator';
										echo '</li>';
									echo '</ul>';
								echo '</div>';
								echo "<div role='tabpanel' class='tab-pane' id='policies{$value['item_id']}'>";
									echo '<ul class="listings-more-content">';
										echo '<li>';
											echo '<i class="fa fa-star" aria-hidden="true"></i>';
											echo '&nbsp;&nbsp;';
											echo 'No Smoking in the room.';
										echo '</li>';
										echo '<li>';
											echo '<i class="fa fa-star" aria-hidden="true"></i>';
											echo '&nbsp;&nbsp;';
											echo 'Pets are not allowed';
										echo '</li>';
										echo '<li>';
											echo '<i class="fa fa-star" aria-hidden="true"></i>';
											echo '&nbsp;&nbsp;';
											echo 'Cancellation and prepayment policies vary according to room type. Please email to us to learn more about it.';
										echo '</li>';
										echo '<li>';
											echo '<i class="fa fa-star" aria-hidden="true"></i>';
											echo '&nbsp;&nbsp;';
											echo 'When booking more than 5 rooms, different policies and additional supplements may apply.';
										echo '</li>';
									echo '</ul>';
								echo '</div>';
							echo '</div>';
						echo '</div>';
						echo '<div class="col-xs-6 col-md-3">';
							$noofimages = count($value['image']);
							if($noofimages) {
								$k=1;
								for($i=1;$i<($noofimages%3)+1;$i++) {
									echo '<div class="row">';
									for($j=1;$j<4;$j++) {
										if (isset($value['image'][$k])) {
											$imgurl = 'http://longtermstay.checkfront.com/'.$value['image'][$k]['path'];
											echo '<div class="listings-img-wrapper col-xs-6 col-sm-4">';
												echo "<a href='{$imgurl}' data-toggle='lightbox'>";
													echo "<img src='{$imgurl}' class='listings-img-gallery'></img>";
												echo '</a>';
											echo '</div>';
										}
										$k++;
									}
									echo '</div>';
								}
							}
						echo '</div>';
					echo '</div>';
				echo '</div>';
			}
			echo "";
		} else {
			echo '<b class="no-results">We could not find any item that matches your criteria, Please broaden your search critieria. (remove location & try.)</b>';
		}
	?>
<input type="hidden" name="_token" value="{{ csrf_token() }}">

</form>
</div>
