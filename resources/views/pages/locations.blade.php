<?php
$start_date = date('m/d/Y'); 
$end_date = date('m/d/Y', strtotime('+30 days'));
?>
<div class='row locations page'> 
	<div class='heading-wrapper row'>
		<h2 class='heading'>Locations</h2>
		<div class='heading-subtext'>Currently we are present at following locations in bay area, California, but we are growing into other cities. Stay tuned!!!</div>
	</div>
	<div class='content-wrapper row'>
		<div class='map-wrapper row'>
			<a href='https://maps.google.com/maps?ll=37.597434,-122.188775&z=10&t=m&hl=en-US&gl=US&mapclient=embed&q=ltstay' class='map-overlay' target='_blank'></a>
			<iframe src="https://www.google.com/maps/embed?pb=!1m16!1m12!1m3!1d43988.818490954916!2d-122.00647047349872!3d37.411549066921445!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!2m1!1sltstay!5e0!3m2!1sen!2sus!4v1458379350934" width="100%" height="494" frameborder="0" style="border:0" allowfullscreen></iframe>
		</div>
		<div class='row map-link'>
			<a href='https://maps.google.com/maps?ll=37.38956,-122.04488&z=15&t=m&hl=en-US&gl=US&mapclient=embed&q=ltstay'>See all locations in Google Maps.</a>
		</div>
		<div class='row'>
			<div class='row'>
				<div class='col-xs-12 col-sm-4'>
					<a class='location-link row' href='<?php echo 'search_validation?start_date='.$start_date.'&end_date='.$end_date.'&location=Sunnyvale' ?>'>
						<div class='ribbon'>
							<span class='ribbon-text'>From $52</span>
						</div>
						<div class='location-text'>
							<h2>Sunnyvale</h2>
						</div>
						<div class='location-overlay'>
							<i class='fa fa-search'></i>
						</div>
						<img class='location-img' src='bootstrap/img/sunnyvale.jpeg'></img>
					</a>
				</div>
				<div class='col-xs-12 col-sm-4'>
					<a class='location-link row' href='<?php echo 'search_validation?start_date='.$start_date.'&end_date='.$end_date.'&location=San Jose' ?>'>
						<div class='ribbon'>
							<span class='ribbon-text'>From $52</span>
						</div>
						<div class='location-text'>
							<h2>San Jose</h2>
						</div>
						<div class='location-overlay'>
							<i class='fa fa-search'></i>
						</div>
						<img class='location-img' src='bootstrap/img/sanjose.jpeg'></img>
					</a>
				</div>
				<div class='col-xs-12 col-sm-4'>
					<a class='location-link row' href='<?php echo 'search_validation?start_date='.$start_date.'&end_date='.$end_date.'&location=Milpitas' ?>'>
						<div class='ribbon'>
							<span class='ribbon-text'>From $52</span>
						</div>
						<div class='location-text'>
							<h2>Milpitas</h2>
						</div>
						<div class='location-overlay'>
							<i class='fa fa-search'></i>
						</div>
						<img class='location-img' src='bootstrap/img/milpitas.jpeg'></img>
					</a>
				</div>
			</div>
			<div class='row'>
				<div class='col-xs-12 col-sm-4'>
					<a class='location-link row' href='<?php echo 'search_validation?start_date='.$start_date.'&end_date='.$end_date.'&location=Fremont' ?>'>
						<div class='ribbon'>
							<span class='ribbon-text'>From $52</span>
						</div>
						<div class='location-text'>
							<h2>Fremont</h2>
						</div>
						<div class='location-overlay'>
							<i class='fa fa-search'></i>
						</div>
						<img class='location-img' src='bootstrap/img/fremont.jpeg'></img>
					</a>
				</div>
				<div class='col-xs-12 col-sm-4'>
					<a class='location-link row' href='<?php echo 'search_validation?start_date='.$start_date.'&end_date='.$end_date.'&location=Santa Clara' ?>'>
						<div class='ribbon'>
							<span class='ribbon-text'>From $52</span>
						</div>
						<div class='location-text'>
							<h2>Santa Clara</h2>
						</div>
						<div class='location-overlay'>
							<i class='fa fa-search'></i>
						</div>
						<img class='location-img' src='bootstrap/img/santaclara.jpeg'></img>
					</a>
				</div>
				<div class='col-xs-12 col-sm-4'>
					<a class='location-link row' href='<?php echo 'search_validation?start_date='.$start_date.'&end_date='.$end_date.'&location=' ?>'>
						<div class='ribbon'>
							<span class='ribbon-text'>From $52</span>
						</div>
						<div class='location-text'>
							<h2>Bay Area</h2>
						</div>
						<div class='location-overlay'>
							<i class='fa fa-search'></i>
						</div>
						<img class='location-img' src='bootstrap/img/sunnyvale.jpeg'></img>
					</a>
				</div>
			</div>
		</div>
	</div>
</div>
