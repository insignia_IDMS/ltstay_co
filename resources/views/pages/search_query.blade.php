<?php
if(Session::has('start_date'))
{
	$start_date = session()->get('start_date'); 
	$end_date = session()->get('end_date');
	$location = session()->get('location');
}

if(!empty($start_date) && !empty($end_date)) {
	echo '<div class="search-query row">';
		echo "<div>You searched for</div>";
		echo "<div>Start Date: <b>$start_date</b>, End Date: <b>$end_date</b>, Location: <b>$location</b></div>";
	echo '</div>';
}
?>
