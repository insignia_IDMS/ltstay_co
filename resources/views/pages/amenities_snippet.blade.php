
<div class="amenities section">
	<div class="heading-wrapper row">
		<h2 class="heading">Best Amenities. Almost All Inclusive.</h2>
	</div>
	<div class="content-wrapper row">
		<div class="col-xs-6 col-sm-3">
			<h3>
				<i class="fa fa-bed fa-lg" aria-hidden="true"></i>
				<span class="heading">Comfort</span>
			</h3>
			<span>We provide comfortable beds, pillows, blankets for every customer. We know our customers, so we have a comfortable office desk and chair for every tenant.</span>
		</div>
		<div class="col-xs-6 col-sm-3">
			<h3>
				<i class="fa fa-play-circle-o fa-lg" aria-hidden="true"></i>
				<span class="heading">Work-Fun</span>
			</h3>
			<span>We have super fast broadband. Be it either office work or home entertainment, we got you covered. We have indian channels along with netflix. Super comfortable sofas and big flat screen tv at every residence.</span>
		</div>
		<div class="col-xs-6 col-sm-3">
			<h3>
				<i class="fa fa-coffee fa-lg" aria-hidden="true"></i>
				<span class="heading">Dining</span>
			</h3>
			<span>Dont have time to cook or eat outside? Want to go Healthy? We provide high quality home made food for lunch and dinner. Still hesitant? we deliver it at your office during weekdays.</span>
		</div>
		<div class="col-xs-6 col-sm-3">
			<h3>
				<i class="fa fa-home fa-lg" aria-hidden="true"></i>
				<span class="heading">Home</span>
			</h3>
				<span>Feel right at home with fully stocked kitchen. Want to cook at weekends? No probs. Inhouse washer-dryer, iron board and all menities to get ready after a relaxed weekend.</span> 
		</div>
	</div>
	<div class="btn-wrapper">
		<a href="{{ url('/amenities') }}" class="btn btn-primary btn-lg">Learn More →</a>
	</div>
</div>
