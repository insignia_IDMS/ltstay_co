<nav class='navbar navbar-default' role='navigation'>
	<div class='container-fluid'>
		<!-- Brand and toggle get grouped for better mobile display -->
    	<div class='navbar-header'>
      		<button type='button' class='navbar-toggle collapsed' data-toggle='collapse' data-target='#bs-example-navbar-collapse-1' aria-expanded='false'>
				<span class='sr-only'>Toggle navigation</span>
				<span class='icon-bar'></span>
				<span class='icon-bar'></span>
				<span class='icon-bar'></span>
			</button>
			<h1 class='reset'>
				<a class='navbar-brand' href="{{ url('/home') }}">Long Term Stay</a>
    		</h1>
		</div>

		<!-- Collect the nav links, forms, and other content for toggling -->
		<div class='collapse navbar-collapse' id='bs-example-navbar-collapse-1'>
      		<ul class='nav navbar-nav navbar-right'>
        		<li class='home'>
					<a href="{{ url('/home') }}"><span class='nav-item'>Home</span></a>
				</li>
				<li class='locations'>
					<a href="{{ url('/locations') }}"><span class='nav-item'>Locations</span></a>
				</li>
				<li class='amenities'>
					<a href="{{ url('/amenities') }}"><span class='nav-item'>Amenities</span></a>
				</li>
				<li class='contact'>
					<a href="{{ url('/contact') }}"><span class='nav-item'>Contact</span></a>
				</li>
			</ul>
		</div><!-- /.navbar-collapse -->
	</div><!-- /.container-fluid -->
</nav>