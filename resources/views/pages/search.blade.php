
<?php

if(Session::has('start_date'))
{
	$start_date = session()->get('start_date'); 
	$end_date = session()->get('end_date');
	$location = session()->get('location');
}
else
{
	$start_date = date('m/d/Y'); 
	$end_date = date('m/d/Y', strtotime('+30 days'));
	$location = '';
}
?>
<div class="search-form">
	<form action="search_validation" class="form-inline" role="form" id="form-search" method="get" accept-charset="utf-8">
		<div class="form-group">
			<label class="search-label" for="inputStartDate">Start Date 
				<span class="required">*</span>
			</label>
			<div class="col-sm-3">
				<input name="start_date" value="<?=$start_date?>" class="form-control start_date " id="start-date" placeholder="mm/dd/yyyy" type="text">
				</div>
			</div>
			<div class="form-group">
				<label class="search-label" for="inputEndDate">End Date 
					<span class="required">*</span>
				</label>
				<div class="col-sm-3">
					<input name="end_date" value="<?=$end_date?>" class="form-control end_date " id="end-date" placeholder="mm/dd/yyyy" type="text">
					</div>
				</div>
				<div class="form-group">
					<label class="search-label" for="inputLocation">Location</label>
					<div class="col-sm-3">
						<input name="location" value="<?=$location?>" class="form-control" placeholder="ex: Sunnyvale" type="text">
						</div>
					</div>
					<div class="form-group">
						<div class="col-sm-3">
							<input name="search" value="Search" class="btn btn-primary search-btn" type="submit">
							</div>
						</div>
					</form>
				</div>