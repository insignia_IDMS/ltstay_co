@extends('layouts.main')

@section('header')
	    @include('pages.top_header')
@endsection

@section('search')
	    @include('pages.search')
@endsection
@section('body')
	    @include('pages.search_query')
        @include('pages.listings')
@endsection
