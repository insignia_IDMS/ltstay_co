@extends('layouts.main')

@section('header')
	    @include('pages.top_header')
@endsection

@section('body')

<div class='about page row'>
	<div class='heading-wrapper row'>
		<h2 class='heading'>About Long Term Stay</h2>
	</div>
	<div class='content-wrapper row'>
		<p>We are a Delaware corporation, operating 14 guesthouses directly managed by us in south bay area providing hospitality solutions to almost hundreds of folks working with leading companies like Apple, Google, Yahoo, Cisco, Facebook, Comcast, Motorola, HP, Samsung etc.</p>
		<p></p>
		<p>We are an established brand having strong value system, customer focus and clear policies. Our mantra is help you concentrate on your core area of business and let us handle everything else in the most professional, hassle-free and tailor-made setting, something we are best at.</p>
	</div>
</div>


@endsection
