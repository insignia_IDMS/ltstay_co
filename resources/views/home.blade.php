@extends('layouts.main')
@section('class_body', 'home')
@section('header')
	    @include('pages.top_header')
@endsection

@section('search')
	    @include('pages.search')
@endsection

@section('booking_carousel')
<div class='hero'>
	<h1 class='hero-text'>Never Miss Your Home.</h1>
	<img src='img/ltstay_hero<?php echo rand(1,6);?>.jpg' alt='long term stay' class='hero-img'></img>
</div>
@endsection

@section('amenities_snippet')
	    @include('pages.amenities_snippet')
@endsection
@section('locations_snippet')
	    @include('pages.locations_snippet')
@endsection
@section('gallery')
	    @include('pages.gallery')
@endsection
