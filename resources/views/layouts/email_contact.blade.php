<div>
<p>Admin,</p>
<div style="padding: 0px 0px 20px 20px;">
    {{$name}}({{$email}}) has sent a message.<br>
    <div style="padding: 10px 0px">
        <b>Message:</b><br>
        {{$bodyMessage}}
    </div>
</div>

Thanks,<br>
LTSTAY
</div>