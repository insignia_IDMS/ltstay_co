<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<title>Welcome to Long Term Stay</title>
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
		<link rel="stylesheet" type="text/css" href="bootstrap/css/bootstrap.min.css">
		<link rel="stylesheet" type="text/css" href="lightbox/css/ekko-lightbox.min.css">
		<link rel="stylesheet" type="text/css" href="font-awesome/css/font-awesome.min.css">
		<link rel="stylesheet" type="text/css" href="css/ltstay.css">
		<link rel="stylesheet" type="text/css" href="jquery-ui/jquery-ui.min.css">
	</head>
	
	<body class="@yield('class_body', 'default')">
		<?php //echo $header; ?>

        @section('header')
        @show  
		
		<div class='main'>

		@if (trim($__env->yieldContent('class_body')))
			<div class='booking-carousel'>
                @section('search')
                @show
                @section('booking_carousel')
                @show
			</div>
                @section('amenities_snippet')
                @show
                @section('locations_snippet')
                @show
                @section('gallery')
                @show
		@else
			@hasSection('search')
				@section('search')
				@show
			@endif
			@section('body')
			@show


					
		@endif

		</div>
		@include('pages.footer')
	</body>
	<script src="js/jquery.min.js"></script>
	<script src="jquery-ui/jquery-ui.min.js"></script>
	<script src="bootstrap/js/bootstrap.min.js"></script>
	<script src="lightbox/js/ekko-lightbox.min.js"></script>
	<script src="js/ltstay.js"></script>
	<script src='bfh/js/bootstrap-formhelpers.js'></script>

	<script>
	$(document).delegate('*[data-toggle="lightbox"]', 'click', function(event) {
		event.preventDefault();
		$(this).ekkoLightbox({
			always_show_close: false,
			keyboard: true
		});
	});
	</script>
</html>
