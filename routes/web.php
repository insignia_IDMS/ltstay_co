<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@home');
Route::get('/home', 'HomeController@home');
Route::get('/locations', 'PagesController@locations');
Route::get('/amenities', 'PagesController@amenities');
Route::get('/contact', 'PagesController@contact');
Route::post('/contact', 'PagesController@postContact');
Route::get('/about', 'PagesController@about');
Route::get('/help', 'PagesController@help');
Route::get('/search_validation', 'PagesController@search_validation');
Route::get('/listings', 'ListingsController@listings');
Route::post('/select', 'ListingsController@select');
Route::get('/book', 'ListingsController@book');
Route::get('/locations', 'PagesController@locations');
Route::get('/locations', 'PagesController@locations');
Route::get('/locations', 'PagesController@locations');


