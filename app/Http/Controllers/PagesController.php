<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Post;
use Mail;
use Session; 

class PagesController extends Controller
{
    public function locations()
    {
           return view('locations'); 
    }

    public function amenities()
    {
           return view('amenities'); 
    }

    public function contact(Request $request)
    {
        //print_r($_POST);
        //exit;
           return view('contact'); 
    }

    public function postContact(Request $request)
    {
        //print_r($_POST);
        //exit;
        //return view('contact'); 
        $data = array(
            'email' => $request->email,
            'name' => $request->name,
            'bodyMessage' => $request->message
        );
        $this->validate($request, ['email' => 'required|email', 'name' => 'min:3', 'message' => 'min:10']);
        
        Mail::send('layouts.email_contact', $data, function($message) use ($data){

            $message->from('mysunilsada@nellorians.com');
            $message->to('mysunilsada@gmail.com');
            $message->subject('ltstay email');   
        });
        /**/
        /*
        Mail::send('layouts.email_contact', $data, function($message){

            $message->to('mysunilsada@gmail.com', 'Testing mail')->subject('Test Mail');
            $message->from('mysunilsada@nellorians.com', 'Mail Test');

        });
        */
        Session::flash('success', 'Email sent successfully');
        //echo "Email sent successfully";
        //exit;
        return redirect('contact');
    }

    public function about()
    {
           return view('about'); 
    }

    public function help()
    {
           return view('help'); 
    }
    
    

    public function search_validation(Request $request)
    {
        //print_r($request->get('start_date'));
        
        if(Session::has('start_date'))
        {
            //exit;
            Session::forget('start_date');
            Session::forget('end_date');
            Session::forget('location');
        }

        session()->put('start_date', $request->get('start_date'));
        session()->put('end_date', $request->get('end_date'));
        session()->put('location', $request->get('location'));

        //echo session()->get('end_date');

        return redirect('listings');
    }

}
